# Integration Tests

B-FAST-specific integration tests are in `tests`. Test data go to
`extroot`, backup dumps to `restore`. Link to both folders from server
profile.
